variable "region" {
  default     = "us-east-1"
}

variable "name_prefix" {
  default = "fargate-basic-example"
}

variable "desired_container_count" {
  default = 2
}

